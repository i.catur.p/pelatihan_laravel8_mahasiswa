<!DOCTYPE html>
<html>
   <head>
       <title>Mahasiswa App</title>
       <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
   </head>
   <body>
       <div class="container">

           <nav class="navbar navbar-inverse">
               <div class="navbar-header">
                   <a class="navbar-brand" href="{{ URL::to('mahasiswa') }}">Mahasiswa App</a>
               </div>
               <ul class="nav navbar-nav">
                   <li><a href="{{ URL::to('mahasiswa/create') }}">Tambah Data</a>
               </ul>
           </nav>

           <h1>Mahasiswa</h1>

           <!-- will be used to show any messages -->
           @if (Session::has('message'))
               <div class="alert alert-info">{{ Session::get('message') }}</div>
           @endif

           <table class="table table-striped table-bordered">
               <thead>
                   <tr>
                       <td>ID</td>
                       <td>Nama</td>
                       <td>Email</td>
                       <td>Jenis Kelamin</td>
                       <td>Actions</td>
                   </tr>
               </thead>
               <tbody>
               @foreach($mahasiswa as $key => $value)
                   <tr>
                       <td>{{ $value->id }}</td>
                       <td>{{ $value->nama }}</td>
                       <td>{{ $value->email }}</td>
                       <td>{{ $value->jenis_kelamin }}</td>

                       <!-- we will also add show, edit, and delete buttons -->
                       <td>

                           {{ Form::open(array('url' => 'mahasiswa/' . $value->id, 'class' => 'pull-right')) }}
                           {{ Form::hidden('_method', 'DELETE') }}
                           {{ Form::submit('Delete this data', array('class' => 'btn btn-warning')) }}
                           {{ Form::close() }}

                           <!-- show the shark (uses the show method found at GET /sharks/{id} -->
                           <a class="btn btn-small btn-success" href="{{ URL::to('mahasiswa/' . $value->id) }}">Delete</a>

                           <!-- edit this shark (uses the edit method found at GET /sharks/{id}/edit -->
                           <a class="btn btn-small btn-info" href="{{ URL::to('mahasiswa/' . $value->id . '/edit') }}">Edit</a>

                       </td>
                   </tr>
               @endforeach
               </tbody>
           </table>
           {{-- Pagination --}}
           <div class="d-flex justify-content-center">
               {{ $mahasiswa->links() }}
           </div>
       </div>
   </body>
</html>