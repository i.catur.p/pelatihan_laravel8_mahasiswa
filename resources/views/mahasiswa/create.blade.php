<!DOCTYPE html>
<html>
<head>
   <title>Shark App</title>
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

   <nav class="navbar navbar-inverse">
       <div class="navbar-header">
           <a class="navbar-brand" href="{{ URL::to('mahasiswa') }}">Mahasiswa App</a>
       </div>
       <ul class="nav navbar-nav">
           <li><a href="{{ URL::to('mahasiswa/create') }}">Tambah Data</a>
       </ul>
   </nav>

<h1>Tambah Data</h1>

<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::open(array('url' => 'mahasiswa')) }}

   <div class="form-group">
       {{ Form::label('nama', 'Nama') }}
       {{ Form::text('nama','' , array('class' => 'form-control')) }}
   </div>

   <div class="form-group">
       {{ Form::label('email', 'Email') }}
       {{ Form::email('email','' , array('class' => 'form-control')) }}
   </div>

   <div class="form-group">
       {{ Form::label('jenis_kelamin', 'Jenis Kelamin') }}
       {{ Form::select('jenis_kelamin', array('0' => 'Select a gender', 'Female' => 'Female', 'Male' => 'Male'), '', array('class' => 'form-control')) }}
   </div>

   <div class="form-group">
       {{ Form::label('alamat', 'Alamat') }}
       {{ Form::textarea('alamat','' , array('class' => 'form-control')) }}
   </div>

   {{ Form::submit('Update Data', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>