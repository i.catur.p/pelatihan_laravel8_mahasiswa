<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\View;
use Validator;
use Session;
use Redirect;


class MahasiswaController extends Controller
{
    public function index()
    {
       // get data paginate 10
       $mahasiswa = Mahasiswa::paginate(10);

       // load the view and pass the sharks
       return View::make('mahasiswa.index')
           ->with('mahasiswa', $mahasiswa);
    }

    public function create()
   {
       return View::make('mahasiswa.create');
   }

   public function store(Request $request)
   {
       // validate
       // read more on validation at http://laravel.com/docs/validation
       $this->validate($request,[
           'nama'       => 'required',
           'email'      => 'required|email',
           'jenis_kelamin' => 'required',
           'alamat' => 'required'
       ]);

           // store
           Mahasiswa::create([
           'nama' => $request-> nama,
           'email' => $request->email,
           'jenis_kelamin' => $request->jenis_kelamin,
           'alamat' => $request->alamat
           ]);
           // redirect
           Session::flash('message', 'Successfully created data!');
           return Redirect::to('mahasiswa');
   }

   public function edit($id)
   {
       // get the data
       $mahasiswa = Mahasiswa::find($id);

       // show the view and pass the shark to it
       return View::make('mahasiswa.edit')
           ->with('mahasiswa', $mahasiswa);
   }

   public function destroy($id)
   {
       // delete
       $mahasiswa = Mahasiswa::find($id);
       $mahasiswa->delete();

       // redirect
       Session::flash('message', 'Successfully deleted the data!');
       return Redirect::to('mahasiswa');
   }

   public function update(Request $request, $id)
   {
       $this->validate($request,[
           'nama'       => 'required',
           'email'      => 'required|email',
           'jenis_kelamin' => 'required',
           'alamat' => 'required'
       ]);

           // update
           $mahasiswa = Mahasiswa::find($id);
           $mahasiswa->nama = $request-> nama;
           $mahasiswa->email = $request->email;
           $mahasiswa->jenis_kelamin = $request->jenis_kelamin;
           $mahasiswa->alamat = $request->alamat;
           $mahasiswa->save();

           // redirect
           Session::flash('message', 'Successfully Update data!');
           return Redirect::to('mahasiswa');
   }


}